# README #

This is the standard POM to be used by the Fiehnlab/WCMC for Java/Scala/Groovy based development and should be updated regularly to ensure it has the correct version.

### What is this repository for? ###

* Quick summary

Contains different profiles to simplify the work with Scala, Groovy and related Frameworks.

### Provided profiles ###

* scala - will be activated, if a folder src/main/scala or src/test/scala exist and provides the scala language support
* groovy - will be activated, if a folder src/main/grovvy exist and provides groovy language support
* docker - will package your application as a docker application, if the file .docker exists
* app - will build an executable jar file, if the file .app exists

### Always activated profiles ###


### How do I get set up? ###

* Summary of set up
* Configuration

To start using and deploying artifacts to our repo, simply edit your $HOME/.m2/settings.xml (linux/mac) or %USERPROFILE%\\.m2\settings.xml (win) file adding the following before any <profile> tag:


```
#!xml
<settings ...>
        <servers>
                <server>
                        <id>releases</id>
                        <username>your_user_name</username>
                        <password>can't tell you</password>
                </server>

                <server>
                        <id>snapshots</id>
                        <username>your_user_name</username>
                        <password>can't tell you</password>
                </server>
        </servers>

        <mirrors>
                <mirror>
                        <!--This sends everything else to /public -->
                        <id>Fiehnlab Nexus Repository</id>
                        <mirrorOf>*</mirrorOf>
                        <url>http://gose.fiehnlab.ucdavis.edu:55000/content/groups/public</url>
                </mirror>
        </mirrors>
        ...
</settings>
```


* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

To deploy the generated artifacts to the Fiehnlab repository, just run:

```
#!bash

mvn deploy
```

This will run all the maven phases (compile, test, package, install) and then upload to the repo.
(You need an account for uploading. Ask Diego)

